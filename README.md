# Programando ordenadores en los 80 y ahora. ¿Qué ha cambiado? (2016)

```plantuml
@startwbs
+[#DarkCyan] Programando ordenadores en los 80 y ahora.
++[#LightPink] Antes 
+++[#Snow] Las computadoras tenian\nmucho menos memoria
+++[#Snow] La velocidad se medía en Hz
+++[#Snow] Usar otro lenguaje que no\nera ensamblador se traducía en\nuna ejecución muy lenta
+++[#Snow] El código se enfocaba en\nser eficiente y ahorrar recrusos
+++[#Snow] Se requeria un apmplio\nconocimiento de la maquina\nen la que se programaba

++[#LightPink] Despues
+++[#Snow] Las computadoras tienen\nmucha mas memoria
+++[#Snow] La velocidad se mide en GHz
+++[#Snow] El conocimiento de las\nplataformas es trabajo\ndel lenguaje compilador
+++[#Snow] El codigo se enfoca\Nen ser entendible por el\nusuario
+++[#Snow] El conocimiento de las\nplataformas es trabajo del\nlenguaje compilador
+++[#Snow] Existen lenguajes que\nni siquiera se ejecutan sobre la\nmaquina base como:
++++[#PaleVioletRed] Java
+++++ Se ejecuta en una\nmaquina virtual

++[#LightPink] Leyes
+++ Moore
++++[#Snow] Cada 18 meses el hardware es el doble de rápido 
+++++[#Snow] Estamos alcanzando límites físicos que\nimpiden este desarrollo

+++ Page
++++[#Snow] Cada 18 meses el software se vuelve el\ndoble de lento 

@endtwbs
```


# Hª de los algoritmos y de los lenguajes de programación (2010)

```plantuml
@startwbs
+[#LightSlateGrey] Hª de los algoritmos y de los lenguajes de programación (2010)

++[#MediumPurple] Lenguajes de Programacion
+++[#PaleVioletRed] Fortran (1959)
++++[#Snow] Lenguaje de programacion\nconocido por usar un\nparadigma imperativo
+++[#PaleVioletRed] List (1960)
++++[#Snow] Lenguaje funcional que\nsirve para simplificar\nexpresiones
+++[#PaleVioletRed] Simula (1962)
++++[#Snow] Primer lenguaje Orientado a Objetos
+++[#PaleVioletRed] Prolog (1971) 
++++[#Snow] Lenguaje de programacion\nlogico e interpretado

++[#MediumPurple] Algoritmos
+++[#Snow] Es un procedimiento\nsistematico y mecanico
+++[#Snow] Costes 
++++[#MintCream] Razonables
+++++[#MintCream]  Son algoritmos los cuales\nsus tiempos de ejecucion\ncrecen despacio
++++[#MintCream]  No Razonables
+++++[#MintCream]  Son algoritmos que duplican\nsus tiempos de ejecucion\al agregar un dato mas
+++[#Snow] Los algoritmos surguieron\nmucho antes que la computacion



@endtwbs
```


# Lenguajes de Programacion y Paradigmas de Programacion

```plantuml
@startwbs
+[#GoldenRod] Lenguajes de Programacion y Paradigmas de Programacion

++[#Grey] Lenguajes de Programacion
+++[#PaleGreen] Debe adaptarse a las Necesidades
+++[#PaleGreen] Ayuda al programador a resolver problemas
+++[#PaleGreen] Buscan ser eficientes
+++[#PaleGreen] Alto Nivel
++++[#PaleVioletRed] C
++++[#PaleVioletRed] C++
++++[#PaleVioletRed] Java
++++[#PaleVioletRed] Python
++++[#PaleVioletRed] Kotlin
++++[#PaleVioletRed] Rubi

+++[#PaleGreen] Bajo Nivel
++++[#PaleVioletRed] Codigo Maquina
++++[#PaleVioletRed] Ensamblador

++[#Grey] Paradigmas de Programacion
+++[#PaleGreen] Es una forma de pensar y abordar\nproblemas utilizando la programacion
+++[#PaleGreen] Tipos
++++[#PowderBlue] Logico
+++++ Usa Mecanismos de inferencia
+++++ Usa recursividad
+++++ Argumentos logicos con\nun solo valor 0 o 1
+++++ Ejemplos de Lenguajes
++++++[#PaleVioletRed] Prolog
++++++[#PaleVioletRed] ALF
++++++[#PaleVioletRed] Mercury
++++[#PowderBlue] Funcional
+++++ Usa Recursividad
+++++ Usa Funciones Matematicas
+++++ Ejemplos de Lenguajes
++++++[#PaleVioletRed] ML
++++++[#PaleVioletRed] HASKELL
++++++[#PaleVioletRed] ERLANG
++++[#PowderBlue] Concurrente
+++++ Usa Recursividad
+++++ Implementa Paralelismo
+++++ Ejemplos de Lenguajes
++++++[#PaleVioletRed] Ada
++++++[#PaleVioletRed] Java

++++[#PowderBlue] Distribuida
+++++ Comunicacion entre ordenadores
+++++ Programas divididos en distintos\nordenadores
+++++ Ejemplos de Lenguajes
++++++[#PaleVioletRed] E
++++++[#PaleVioletRed] Limbo
++++++[#PaleVioletRed] Oz

++++[#PowderBlue] Orientada a Objetos
+++++ Utiliza la Recursividad
+++++ Implementa la Abstraccion
+++++ Tiene distintos tipos de datos
+++++ Se programa en clases
+++++ Ejemplos de Lenguajes
++++++[#PaleVioletRed] Java
++++++[#PaleVioletRed] Php
++++++[#PaleVioletRed] JavaScript


@endtwbs
```